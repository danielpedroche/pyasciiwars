import curses
import random

version = "1.0.0 RC1.1"
print "Py-wars ascii version " + version
print "Thanks for playing"

scene = curses.initscr()
curses.curs_set(0)

wy, wx = scene.getmaxyx()
w = curses.newwin(wy, wx, 0, 0)

w.keypad(1)
w.timeout(100)


class Disparo:
    def __init__(self, type):
        self.disparos = []
        self.type = type

    def shoot(self, X, Y):
        self.disparos.insert(0,[X+2,Y])
        self.disparos.insert(0,[X+4,Y])
        self.disparos.insert(0,[X+6,Y])

    def render(self):
        s=0
        for i in self.disparos:
            i[1]-=1
            if i[1] <= 0:
                del self.disparos[s] 
            else: 
                w.addch( i[1], i[0], self.type[0] )
            s+=1

class Nave:
    def __init__(self, disparos,X,Y):
        self.shape = "(-oOo-)"
        self.X = X - (len(self.shape)/2)
        self.Y = Y
        self.gun = Disparo("!")
        
    def shoot(self):
        self.gun.shoot(self.X, self.Y)

    def render(self):
        s=0
        for i in self.shape:
            s+=1
            w.addch( self.Y, self.X+s, i )

class Enemigo:
    def __init__ (self):
        self.shape = "|O|"
        self.X = random.randint(1,wx-1-len(self.shape)) 
        self.Y = 1

    def render(self):
        s=0
        for i in self.shape:
            s+=1
            w.addch( self.Y, self.X+s, i )
        if self.Y < wy-4:
            self.Y+=random.randint(0,1)
            return 0
        else:
            self.Y = 1
            self.X = random.randint(1,wx-1-len(self.shape))
            return 1 

    def checkCollision(self, heroe):
        mypos = [self.X+len(self.shape)/2+1, self.Y]
        if mypos in heroe.gun.disparos:
            self.X = random.randint(1,wx-1-len(self.shape)) 
            self.Y = 1
            return 1
        else:
            return 0

class Mundo:
    def __init__(self):
        self.enemigosVivos = 0
        self.enemigosMuertos = 0
        self.disparos = 0
    
    def printScoreboard(self):
        bar=""
        i=0
        while i < wx-2:
            bar += "-"
            i+=1
        w.addstr(wy-1, 1, bar )
        w.addstr(wy-4, 1, bar )

        if self.disparos > 0:
            w.addstr(wy-3, 4, "Tasa de acierto {:4.2f}%".format((float(self.enemigosMuertos)/float(self.disparos))*100))
        else:
            w.addstr(wy-3, 4, "Tasa de acierto 0%")

        w.addstr(wy-2, 4, "Disparos {} - Enemigos vivos {} - Enemigos muertos {}".format(self.disparos,self.enemigosVivos,self.enemigosMuertos))

def main():
    Jugador = Nave(100, wx/2, wy/2)
    Malote = Enemigo()
    GUI = Mundo()

    key = curses.KEY_RIGHT

    while True:
        next_key = w.getch()
        key = key if next_key == -1 else next_key

        if key == curses.KEY_BACKSPACE:
            curses.endwin()
            quit()

        if key == curses.KEY_RIGHT:
            if Jugador.X < wx-len(Jugador.shape)-1:
                Jugador.X+=1
        if key == curses.KEY_LEFT:
            if Jugador.X > 0:
                Jugador.X-=1
        if key == curses.KEY_UP:
            if Jugador.Y > 1:
                Jugador.Y-=1
        if key == curses.KEY_DOWN:
            if Jugador.Y < wy-5:
                Jugador.Y+=1
        if key == 10 or key == curses.KEY_ENTER or key == 13 or key == 32:
            GUI.disparos+=1
            Jugador.shoot()

        w.clear()
        Jugador.render()
        Jugador.gun.render()
        GUI.enemigosMuertos+=Malote.checkCollision(Jugador)
        GUI.enemigosVivos+=Malote.render()
        GUI.printScoreboard()

main()

